# NServiceBus.Course #

Slides and exercices for the NServiceBus Training

### How to install the temporal liecense? ###

* [Install the Powershell module](https://github.com/particular/NServiceBus.Powershell/releases/latest)
* Run Powershell as administrator
* Type: Import-Module NServiceBus.PowerShell
* Type: Install-NServiceBusPlatformLicense